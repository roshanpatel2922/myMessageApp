//
//  Home.swift
//  myMessageApp
//
//  Created by Roshan Patel on 11/10/22.
//

import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseCore
import FirebaseFirestore


struct User {
    var Name = ""
    var uid = ""
    var phonenumber = ""
    
    init(dic: [String:Any]) {
        self.Name = dic["userName"] as? String ?? ""
        self.uid = dic["uId"] as? String ?? ""
        self.phonenumber = dic[ "MO. No"] as? String ?? ""
    }
}

class Home: UIViewController, UITableViewDelegate, UITableViewDataSource {
  
    @IBOutlet weak var mytblView: UITableView!
    var datasss = Firestore.firestore()
    var userarr = [User]()
    override func viewDidLoad() {
        super.viewDidLoad()
        mytblView.delegate = self
        mytblView.dataSource = self
        let aa = datasss.collection ("Profile")
        aa.getDocuments { i, error in
            let ab = i?.documents
            ab?.forEach({ ii in
            
            
                self.userarr.append(User(dic: ii.data()))
                self.userarr = self.userarr.filter ({ i in
                    i.uid != ID
                })
                self.mytblView.reloadData()
            })
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        userarr.count
    }
                    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HomeTBCell
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        Helper.shared.newThread(SecID: userarr[indexPath.row].uid)
        self.mytblView.reloadData()
    }

}
                    

    

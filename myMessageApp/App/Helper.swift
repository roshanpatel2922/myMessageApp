//
//  Helper.swift
//  myMessageApp
//
//  Created by Roshan Patel on 29/01/23.
//
import UIKit
import FirebaseStorage
import FirebaseAuth
import FirebaseCore
import FirebaseFirestore

class Helper {
    static var shared = Helper()
    var AllData = Firestore.firestore()
    private init() {
        
    }
    
    func newProfile(nameProfile : String) {
        let a = self.AllData.collection("profile").document(ID)
        a.setData(["userName" : nameProfile, "uid" : ID, "NO.No" : "9165706351"])
    }
    
    func newThread(SecID : String ) {
        let b = AllData.collection("Threads").document()
        b.setData(["users" : [ID,SecID],"Time" : Date(), "Title" : "Text..", "DocId" : b.documentID])
    }
    func messagesss(nameMessage : String) {
        let c = self.AllData.collection("Message").document()
        c.setData(["Message" : nameMessage, "Date" : Date(), "DocID" : ID ])
    }
}

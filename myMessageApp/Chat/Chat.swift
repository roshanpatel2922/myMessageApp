//
//  Chat.swift
//  myMessageApp
//
//  Created by Roshan Patel on 11/10/22.
//

import UIKit

class Chat: UIViewController, UITableViewDelegate, UITableViewDataSource, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        return cell
    }
    
    var name = ""
    var mychat = [String]()
    @IBOutlet weak var myTblview: UITableView!
    @IBOutlet weak var profView: UIView!
    @IBOutlet weak var mytxtView: UIView!
    @IBOutlet weak var profimg: UIImageView!
    @IBOutlet weak var imgPickBtn: UIButton!
    @IBOutlet weak var sendbtn: UIButton!
    @IBOutlet weak var mytxtfield: UITextField!
    @IBOutlet weak var ProfName: UILabel!
    
    var n : Int = -1
    var forImage = [Int]()
    var allData = [DataBase]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myTblview.delegate = self
        myTblview.dataSource = self
        myTblview.layer.cornerRadius = 20
        mytxtView.layer.cornerRadius = 20
        mytxtfield.layer.cornerRadius = 20
        sendbtn.layer.cornerRadius = 10
        profView.layer.cornerRadius = 20
        profimg.layer.cornerRadius = 20
        imgPickBtn.layer.cornerRadius = 33
        //        myTblview.reloadData()
        //        getData()
        if allData.isEmpty != true {
            myTblview.scrollToRow(at: IndexPath(row: allData.count-1, section: 0), at: .bottom, animated: true)
        }
        //
    }
    
    @IBAction func OnClickBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func OnClickimgpick(_ sender: Any) {
        let image = UIImagePickerController()
        image.sourceType = .photoLibrary
        image.delegate = self
        present(image, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        
        if let image = info[.originalImage] as? UIImage {
            profimg.image = image
        }
    }
    @IBAction func onClickTxtfield(_ sender: Any) {
        if mytxtfield.text != "" {
            sendbtn.isSelected = true
        } else {
            sendbtn.isSelected = false
        }
    }
    
    @IBAction func onClickSend(_ sender: Any) {
        if mytxtfield.text != "" {
            var whiteSpaceee = mytxtfield.text?.trimmingCharacters(in: .whitespaces)
            if whiteSpaceee != "" {
            }
            //            whiteSpaceee = ""
            mytxtfield.text = ""
            onClickTxtfield(UITextField())
            myTblview.reloadData()
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            mychat.count
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let forDate = DateFormatter()
            forDate.dateFormat = "hh:mm a"
            
            if allData[indexPath.row].name != "Roshan" {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell2", for: indexPath) as! ChatCell2
                cell.userLbl.text = allData[indexPath.row].data
                cell.Usertime.text = forDate.string(from: allData[indexPath.row].date!)
                cell.username.text = allData[indexPath.row].name
                //            cell.userImg.isHidden = forShowImage(ip: indexPath)
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "ChatCell1", for: indexPath) as! ChatCell1
                cell.ChatLbl.text = allData[indexPath.row].data
                cell.myTime.text = forDate.string(from: allData[indexPath.row].date!)
                
                return cell
            }
            
        }
        
    }
    
}


//
//  List.swift
//  myMessageApp
//
//  Created by Roshan Patel on 11/10/22.
//

import UIKit

class List: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var myTblView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        myTblView.delegate = self
        myTblView.dataSource = self
        myTblView.layer.cornerRadius = 20
    
    }
    let arr = ["Roshan Patel", "Kishan Jogani", "Manav Jalodara", "Brijesh Talavia"]
    let randmsg = ["Hey! I am using my chat", "Typing...", "I am Busy Now", "Buisness Account"]
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        arr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ListCell", for: indexPath) as! ListCell
        cell.profname.text = arr[indexPath.row].description
        cell.profmsg.text = randmsg[indexPath.row].description
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let a = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "Chat") as! Chat
        a.name = arr[indexPath.row]
        self.tabBarController?.navigationController?.pushViewController(a, animated: true)
    }
}

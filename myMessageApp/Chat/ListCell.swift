//
//  ListCell.swift
//  myMessageApp
//
//  Created by Roshan Patel on 11/10/22.
//

import UIKit

class ListCell: UITableViewCell {

    @IBOutlet weak var profimg: UIImageView!
    
    @IBOutlet weak var profname: UILabel!
    
    @IBOutlet weak var profmsg: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

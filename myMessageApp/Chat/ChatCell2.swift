//
//  ChatCell2.swift
//  myMessageApp
//
//  Created by Roshan Patel on 11/10/22.
//

import UIKit

class ChatCell2: UITableViewCell {

    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var userLbl: UILabel!
    @IBOutlet weak var Usertime: UILabel!
    @IBOutlet weak var userImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

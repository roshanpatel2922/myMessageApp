//
//  Settings.swift
//  myMessageApp
//
//  Created by Roshan Patel on 29/01/23.
//

import UIKit

class Settings: UIViewController {

    @IBOutlet weak var myTblVIEw: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        myTblVIEw.layer.cornerRadius = 20
    }
    
    @IBAction func profile(_ sender: UIButton) {
        let alertCr = UIAlertController(title: "Add Profile", message:  "", preferredStyle: .alert)
        alertCr.addTextField()
        
        let ab = alertCr.textFields?[0]
        let alertA = UIAlertAction(title: "Done", style: .default) { _ in
            if ab?.text != "" {
                Helper.shared.newProfile(nameProfile: ab!.text!)
                Helper.shared.newThread(SecID: ab!.text!)
            }
        }
        alertCr.addAction(alertA)
        self.present(alertCr, animated: true)
    }
    
}

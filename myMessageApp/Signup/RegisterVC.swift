//
//  RegisterVC.swift
//  myMessageApp
//
//  Created by Roshan Patel on 03/02/23.
//

import UIKit

class RegisterVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func OnClickReg(_ sender: Any) {
        let a = UIStoryboard(name: "Signup", bundle: nil).instantiateViewController(withIdentifier: "SignupVC") as! SignupVC
        self.navigationController?.pushViewController(a, animated: true)
    }
    
}
